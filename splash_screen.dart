// ignore: file_names
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:middleman/Module%203%20Screens/Login%20Screen.dart';

// ignore: camel_case_types
class splashScreen extends StatefulWidget {
  const splashScreen({ Key? key }) : super(key: key);

  @override
  State<splashScreen> createState() => _splashScreenState();
}

// ignore: camel_case_types
class _splashScreenState extends State<splashScreen> {
  @override
  void initState(){
    super.initState();
    Timer(const Duration(seconds: 5),()=>Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const LoginPage(),)));

    
  }
  @override
  Widget build(BuildContext context) {
     return MaterialApp(
       theme: ThemeData(
         brightness: Brightness.dark,
       ),
       home: Scaffold(
       appBar: AppBar(
       title: const Text("Splash Screen"),
       
     ),
     body: Center(
       child: Column(
         crossAxisAlignment: CrossAxisAlignment.center,
         mainAxisAlignment: MainAxisAlignment.spaceAround,
         // ignore: prefer_const_literals_to_create_immutables
         children: [
          const Image(image: AssetImage('Assets/splash_image.jpg'),
           ),
           const Text("Splash Screen",
           textAlign: TextAlign.center,
           style: TextStyle(
             color: Colors.green,
             fontWeight: FontWeight.bold,
             fontSize: 20.0,
           ),
           )
         ],
       )
     ),
       ),  
    );
  }
}