// ignore_for_file: file_names
import 'package:flutter/material.dart';

import 'Dashboard Screen.dart';
// ignore: camel_case_types
class usereditPage extends StatefulWidget {
  const usereditPage({ Key? key }) : super(key: key);

  @override
  State<usereditPage> createState() => _usereditPageState();
}

// ignore: camel_case_types
class _usereditPageState extends State<usereditPage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
       theme: ThemeData(
         brightness: Brightness.dark,
       ),
       home: Scaffold(
     appBar: AppBar(
       title: const Text("User Profile Edit Page"),
       
     ),
     
     body: Center(
       child: ListView(
          padding: const EdgeInsets.symmetric(horizontal: 18.0),
          children: <Widget> [
            Column(
              // ignore: prefer_const_literals_to_create_immutables
              children:<Widget> [
                const SizedBox(height: 120,),
                const Text("EDIT YOUR PROFILE" ,style: TextStyle(fontSize: 20.0,color: Colors.blue,))
              ],
            ),

             const SizedBox(height: 60.0,),
            const TextField(
              decoration: InputDecoration(
                labelText: "First Name:",
                labelStyle: TextStyle(fontSize: 20.0,color: Colors.white),
                filled: true,
               )
            ),

             const SizedBox(height: 20.0,),
            const TextField(
              decoration: InputDecoration(
                labelText: "Surname Name:",
                labelStyle: TextStyle(fontSize: 20.0,color: Colors.white),
                filled: true,
               )
            ),

             const SizedBox(height: 20.0,),
            const TextField(
              decoration: InputDecoration(
                labelText: "Email:",
                labelStyle: TextStyle(fontSize: 20.0,color: Colors.white),
                filled: true,
               )
            ),

            const SizedBox(height: 20.0,),
            const TextField(
              obscureText: true,
              decoration: InputDecoration(
                labelText: " Enter Password:",
                labelStyle: TextStyle(fontSize: 20.0,color: Colors.white),
                filled: true,
              ),
            ),

            const SizedBox(height: 20.0,),
            const TextField(
               obscureText: true,
              decoration: InputDecoration(
                labelText: "Re-enter Passord:",
                labelStyle: TextStyle(fontSize: 20,color: Colors.white),
                filled: true,
               )
            ),

             const SizedBox(height: 20.0,),
          Column(
            children: <Widget> [
              ButtonTheme(height: 80.0,
               disabledColor: Colors.green,
                child: const ElevatedButton(
                 onPressed: null, child: Text("SAVE",style: TextStyle(fontSize: 20.0,
                 color: Colors.green),),
               ),
               ),

               const SizedBox(height: 20.0,),
        Center(
        // ignore: prefer_const_literals_to_create_immutables
        child: Column(children: <Widget>[
        GestureDetector(
          child: const Text("Go to Home Page",style: TextStyle(fontSize: 20.0,color: Colors.blue),),
           onTap: () {
             Navigator.push(context, MaterialPageRoute(builder: (context) => const dashboardPage(), ));
            }
           )
           ]
          )
          ),
            ],
          ),
           
          
          ],
        ),
      
          
     ),
       ) 
    );
  }
}