import 'package:flutter/material.dart';
import 'package:middleman/Module%203%20Screens/splash%20screen.dart';
import 'Module 3 Screens/Dashboard Screen.dart';
import 'Module 3 Screens/FeatureScreen 1.dart';
import 'Module 3 Screens/FeatureScreen 2.dart';
import 'Module 3 Screens/Login Screen.dart';
import 'Module 3 Screens/Registration Screen.dart';
import 'Module 3 Screens/UserProfile EditScreen.dart';

void main() {
  runApp( const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  MaterialApp(    
      initialRoute: "/splashscreen",
      routes: {
        "/":(context) => const LoginPage(),
        "/dashboard" :(context) => const dashboardPage(),
        "/registration" :(context) => const registrationPage(),
        "/featurescreen1" :(context) => const featurescreen1Page(),
        "/featurescreen2" :(context) => const featurescreen2Page(),
        "/useredit" :(context) => const usereditPage(),
         "/splashscreen" :(context) => const splashScreen(),
       
      },
      
    );
  }
}


