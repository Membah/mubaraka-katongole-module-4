// ignore_for_file: file_names
import 'package:flutter/material.dart';

import 'FeatureScreen 2.dart';
// ignore: camel_case_types
class featurescreen1Page extends StatefulWidget {
  const featurescreen1Page({ Key? key }) : super(key: key);

  @override
  State<featurescreen1Page> createState() => _featurescreen1PageState();
}

// ignore: camel_case_types
class _featurescreen1PageState extends State<featurescreen1Page> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
       theme: ThemeData(
         brightness: Brightness.dark,
       ),
       home: Scaffold(
       appBar: AppBar(
       title: const Text("Feature Screen 1 Page"),
       
     ),
     body: Center(
       child: ListView(
         padding:  const EdgeInsets.symmetric(horizontal: 18.0),
            children: <Widget> [
            Column(
              // ignore: prefer_const_literals_to_create_immutables
              children:<Widget> [
                const SizedBox(height: 120,),
                const Text("Feature Screen 1" ,style: TextStyle(fontSize: 25,color: Colors.black),),
                const SizedBox(height: 150,),
                 ElevatedButton(
             child: const Text("Go To Feature Screen 2"),
            onPressed: () {
             Navigator.push(context,MaterialPageRoute(builder: (context) => const featurescreen2Page(), ));
            }
            ),
              ],
            ),
            ] 
            
       )
     ),
       ) 
    );
  }
}