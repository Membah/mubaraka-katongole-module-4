// ignore_for_file: file_names
import 'package:flutter/material.dart';

import 'Dashboard Screen.dart';
// ignore: camel_case_types
class featurescreen2Page extends StatefulWidget {
  const featurescreen2Page({ Key? key }) : super(key: key);

  @override
  State<featurescreen2Page> createState() => _featurescreen2PageState();
}

// ignore: camel_case_types
class _featurescreen2PageState extends State<featurescreen2Page> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
       theme: ThemeData(
         brightness: Brightness.dark,
       ),
       home: Scaffold(
       appBar: AppBar(
       title: const Text("Feature Screen 2 Page"),
       
     ),
       body: Center(
       child: ListView(
         padding:  const EdgeInsets.symmetric(horizontal: 18.0),
            children: <Widget> [
            Column(
              // ignore: prefer_const_literals_to_create_immutables
              children:<Widget> [
                const SizedBox(height: 120,),
                const Text("Feature Screen 2" ,style: TextStyle(fontSize: 25,color: Colors.black),),
                const SizedBox(height: 150,),
                 ElevatedButton(
             child: const Text("Go To Dashboard"),
            onPressed: () {
             Navigator.push(context,MaterialPageRoute(builder: (context) => const dashboardPage(), ));
            }
            ),
              ],
            ),
            ] 
            
       )
     ),
       )
    );
  }
}