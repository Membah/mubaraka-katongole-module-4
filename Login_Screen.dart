// ignore_for_file: file_names

import 'package:flutter/material.dart';

import 'Dashboard Screen.dart';
import 'Registration Screen.dart';


class LoginPage extends StatefulWidget {
  const LoginPage({ Key? key }) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
       theme: ThemeData(
         brightness: Brightness.dark,
       ),
       home: Scaffold(
      appBar: AppBar(
        title: const Text("Login Page"),
      ),

      body: Center(
        child: ListView(
          padding: const EdgeInsets.symmetric(horizontal: 18.0),
          children: <Widget> [
            Column(
              // ignore: prefer_const_literals_to_create_immutables
              children:<Widget> [
                const SizedBox(height: 120.0,),
                const Text("WELCOME USER" ,style: TextStyle(fontSize: 25,color: Colors.blue),)
              ],
            
            ),

             const SizedBox(height: 60.0,),
            const TextField(
              decoration: InputDecoration(
                labelText: "User Name:",
                labelStyle: TextStyle(fontSize: 20,color: Colors.white),
                filled: true,
               )
            ),
            
            const SizedBox(height: 20.0,),
            const TextField(
              obscureText: true,
              decoration: InputDecoration(
                labelText: "Password:",
                labelStyle: TextStyle(fontSize: 20.0,color: Colors.white),
                filled: true,
              ),
            ),

            const SizedBox(height: 50.0,),
          Column(
            children: <Widget> [
              ButtonTheme(height: 80.0,
                child: ElevatedButton(                
                child: const Text("LOGIN",style: TextStyle(fontSize: 20.0,color: Colors.white)),
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => const dashboardPage() ,));
                }, 
                                             
               ),
               ),

               const SizedBox(height: 30.0,),
             Center(
        // ignore: prefer_const_literals_to_create_immutables
        child: Column(children: <Widget>[
        GestureDetector(
          child: const Text("New User? Register Here",style: TextStyle(fontSize: 20.0,color: Colors.green),),          
           onTap: () {
             Navigator.push(context, MaterialPageRoute(builder: (context) => const registrationPage(), ));
            }
           )
           ]
          )
          ),
            ],
          ),
          ],
        ), ),
       )
    );
  }
}
   